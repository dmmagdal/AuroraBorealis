# testipv6SocketServer.py
# Author: Diego Magdaleno
# Tests whether a socket web server can be reached from across the
# internet using IPv6.
# Python 3.7


import socket
import threading
import sys
import asyncio
import websockets


'''
class Server:
	peers = []
	connections = []

	def __init__(self):
		sock = socket(socket.AF_INET6, socket.SOCK_STREAM)
		sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		sock.bind("::", 8888)
		sock.listen(10000)

		while True:
			conn, addr = sock.accept()
			conn_thread = threading.Thread(
				target=self.handler, args=(conn, addr)
			)

			conn_thread.daemon = True
			conn_thread.start()

			self.connections.append(conn)
			self.peers.append(addr[0])
			self.sendPeers()
			print(f"{addr[0]}: {addr[1]}: connected")


	def handler(self, conn, addr):
		pass


	def sendPeers(self):
		pass
'''

async def greeting(websocket, path):
	name = await websocket.recv()
	print("< {}".format(name))

	greeting = "Hello {}".format(name)
	await websocket.send(greeting)
	print("> {}".format(greeting))


def main():
	print("Starting web server...")

	# Get IPv6 address(es).
	# getaddrinfo returns a list of 5 tuples with the following
	# structure:
	# (family, type, proto, canonname, sockaddr)
	# sockaddr is a tuple describing a socket address whose format
	# depends on the returned family. 2 tuple (address, port) for
	# AF_INET. 4 tuple (address, port, flowinfo, scope_id) for
	# AF_INET6. sockaddr is meant to be passed to the socket.connect()
	# method.
	ipv6_addrs = socket.getaddrinfo(
		socket.gethostname(), 8888, family=socket.AF_INET6
	)
	print("IPv6 address(es):")
	for addr in ipv6_addrs:
		print(f"\t{addr}")

	print(addr[4])

	#while True:
	#start_server = websockets.serve(greeting, "")

	# Create socket object.
	sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
	print("Socket successfully created")

	# Bind socket to port.
	#sock.bind((addr[4][0], 8888))
	sock.bind(("::1", 8888))
	sock.listen(5)

	while True:
		conn, addr = sock.accept()
		print("Connected to {}".format(addr))

		conn.send("Thank you for connecting".encode())

		conn.close()
		break


	print("Shutting down...")
	exit(0)


if __name__ == "__main__":
	main()