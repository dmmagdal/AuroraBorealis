# OSINT (Project Name Missing)


### Description

This project is an open source, distributed software platform that leverages OSINT (Open Source Intelligence) sources to enable the Projects of Interest and similar communities to develop their own Person of Interest related projects.


### Break Down

The platform is to be broken down into 3 main componentds:

- Data
- UI
- Applications

 > #### Data

 > > Everything to do with the data that feeds to the rest of the platform. This section can be further broken up into 3 additional sections consisting of **collection**, **cleaning**, and **storage**. In data **collection**, community members develop and utilize APIs and web scraping to obtain raw data from their sources on the web. Since this project relies on OSINT resources, there are set standards as to what feeds are allowed as part of the project. Within **cleaning**, the captured data is processed into more universal formats that makes it easier to load and store. **Storage** is handled by distributing the data into shards and replicas across the network. Ideally the storage is handled in a way similar to the BitTorrent network.

 > #### UI

 > > The client side interface that is handled through Kaki's Thornhill OS. Allows users abilities such as search, view programs & feeds, run applications, toggle settings, etc.

 > #### Applications

 > > Applications are where the community can use the data obtained by the platform to develop their own projects. While these projects do not directly have to relate to the Person of Interest show, we the creators feel that this platform is a major stepping stone in replicating or emulating the content scene in that series. 

### Network

The network is proposed to be entirely peer-to-peer with two prevailing structures in mind. Both structures involve dividing the network into shard fragments where each shard is responsible for carrying out a set amount of tasks (tasks being some program as part of the **data** component or hosting services on the **application** component). Most shards spend their traffic communicating with its members to mantain synchronization of data in their buffers before agreeing on a universal "truth" and writing that data to storage. Shards can communicate with other members across the network to update the system on shard and overall network health, synchronizing stored data, or querying data across shards for applications.

Structural design diverges in the following:

 - One design furthers to maintain there is no centralization in any way. Shard members are treated equally with no preference.

 - The second design is a tiered system where shards are created by region to enable lower latency. In addition, each shard has a set "master" node that is elected as the node with the lowest latency to all its members. These master nodes communicate to their members resource allocation for tasks (again, both **data** and **application** programs) as well as communicate to each other within the larger network.

### Assorted Design Requirements

**This section exists as a temporary way to store additional design requirements that do not fit an existing category**

### Project Status & Roadmap

### Node Data

node:{
    shard_id: "",
    node_id: "",
    node_address: "",
    role: "",
    jobs: [job,],
}
view:{
    node,
}
shard_members:{
    node
}
job:{
    name: "",
    description: "",
    job_id: "",
    role: [data, application],
    resources: {
        min_memory: "",
        max_memory: "",
        num_threads: "",
        num_processes: "",
        gpu_enabled: "",
    },
    author: "",
    shard_id: "", <---- (maybe?)
}
jobs_list:{
    job,
}

### Roles/Job Assignment

Master Jobs List
 > List distributed to all nodes
 > Scale up based on number of available shards

### Shards

All shards have a shared min_size and max_size in the number of nodes they can contain
 > max_size is to make sure shard network traffic is not overloaded.
 > min_size is to make sure there are sufficient nodes in a shard to keep it alive.

Shard_id assignments to jobs and nodes ais dynamic
 > A shard_id must exist within the network otherwise the value assigned to a node is changed.

Shards split when they reach max_size
 > New shard gets an idea of max(shard_ids) + 1
 > New shard size >= min_size
 > overloaded shard splits like mitosis to create new shard.

Shards have a limited job queue size
 > Relating to recovering from the "Doomsday" scenario, a shard can only take on so many jobs from the given master jobs list.
 > It is possible for a shard to never reach its queue size limit if it takes on jobs with high resource requirements.

### Problematic Faults

#### The "Doomsday" Scenario

Premise: The entire OSINT network goes down at the same time for whatever reason. A single node comes online to try and rebuild the network and restart it.
 > Node attempts to rebuild feeds, restart all jobs, and load all previously saved data.
 > Node attempts to find other nodes (either no others are online or they can't find each other).

 Problem/Resolution: Node burnout. A single node cannot run all jobs on the OSINT network, especially as the number of jobs on the network grows along with the way the data is broken up across the network. The following design ideas could help minimize the risk of a node burning out:
  > Priotize or only run a select n number of jobs (ie the first 100 jobs within the sorted jobs list). Sort the master jobs list such that jobs with lower (and therefore the earlier) job_ids are prioritized as jobs to run. Alternatively, the council will select a set number of jobs from the master jobs list that will be set as the highest priority jobs that ensure the most utility out of the network.
  > Relating to the once stored data across the network (however it was designed to be stored), the node will disregard or erase any data stored that is not related to the prioritized jobs listed. It must be accepted that any previous historical data collected from other programs may be lost forever (even if network is somehow restored).

  This scenario is easier to recover from in the infancy of the network/platform. The ecosystem of applicatons and data collection programs would be small and light. As the network scales up and more intensive programs come online, this scenario becomes harder to recover from (recovery also including maintaining up to date and synchronized data).

### Possible Programming Languages

Each language has its own strengths and weaknesses listed below. The best one will be chosen to build the network backbone in.

 > Dart
 > > Pros:
 > > Cons: Supported by Google. Made primarily for mobile applications.
 > Go
 > > Pros:
 > > Cons: Supported by Google
 > C/C++
 > > Pros:
 > > Cons: Excessively low level.
 > NodeJS
 > > Pros:
 > > Cons:

### Node Connection Flowmap

```
          [Node Connects]
                ||
                \/
      [Node has a shard_id?]
     ||                    ||
    (yes)                 (no)
     ||                    ||
     \/                    \/
[Does a shard          [Assign to
exist w/      --(no)--> a shard]
shard_id?]
     ||                    ||
     \/                    \/
     -----------------------
                ||
                \/
        [Connect to shard]
                ||
                \/
              [Sync]
```

### Data Scope

#### Global Scope

View [Node, Node, ... ]
Master Jobs List [Job, Job, ... ]

#### Shard Scope

Shard Members [Node, Node, ... ]
Shard Jobs [Job, Job, ... ]

#### Node Scope

Node (see node datastructure above)
Node Jobs [Job, Job, ... ] (should be the same as Shard Jobs?)

### Data Synchronization

#### File Sync

 > A scraper on a node writes their data to their local storage.
 > The local storage is hashed.
 > Concensus between nodes is established by accepting the hash with the highest number of instances. If there is no hash that stands out (all hashes are unique), then concensus is reached through election.
 > The storage associated with the winning hash is propagated.

Note: Election is a process where a node randomly selects a peer in a list of its peers and that selection is cast as a vote. The peer with the most votes is elected the winner. The election process will repeat until there is a clear winner in the event of ties. Statistically speaking, there is a low probability of waiting long for election results.

Note: A downside of this strategy as it is currently described is that the file transfer when storage is propagated puts strain on the network bandwidth. 

#### Buffer Sync

 > A scraper on a node writes their data to a buffer.
 > Concensus between nodes is established by accepting the buffer data that appears the most. If there is no clear winner, then rely on election.
 > The winning data is propogated to all nodes and written to storage.

 Note: A downside of this stragety is that it requires developers to use functions or calls to the core networking code to use the buffer before writing to the storage. This will cause confusion and frustration with developers if this process is not straightforward.

 ### Design Questions That Need Answers

 How should jobs (programs) be formatted (especially if using docker)?
 How will the network store/retrieve jobs and their associated code & data?
 How will data be formated (databases, directories, files)?
 How will jobs keep track of what is in their storage? 