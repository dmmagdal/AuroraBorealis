# testipv6REST.py
# author: Diego Magdaleno
# Tests whether a REST server can be reached from across the internet
# using IPv6.
# Python 3.7


import json
import socket
import ipaddress
from flask import Flask, jsonify, request
from flask_restful import Resource, Api


class Route(Resource):
	def get(self):
		ipv6_addrs = socket.getaddrinfo(
			socket.gethostname(), 8888, family=socket.AF_INET6
		)

		# Return IP address data of server and host.
		return jsonify({
			"host_ip": request.remote_addr,
			"server_ip": ipv6_addrs,
		})


# Initialize app and api.
app = Flask(__name__)
api = Api(app)

# Add resources (routes) directing to classes.
api.add_resource(Route, "/routes")


if __name__ == '__main__':
	#app.run(host="0.0.0.0", port=8888, debug=True)
	app.run(host="::", port=8888, debug=True)